import json
import sys

high_vulns = 0
medium_vulns = 0
go_vulns = 0
with open('semgrep-report.json', 'r') as f:
    hardcoded_secrets_flag = False
    results = json.load(f).get('results', [])
    for result in results:
        metadata = result.get('extra', {}).get('metadata', {})
        cwe = metadata.get('cwe')
        if isinstance(cwe, list):
            for cwe_item in cwe:
                if 'CWE-798' in cwe_item:
                    hardcoded_secrets_flag = True
                    break
        elif 'CWE-798' in cwe:
            hardcoded_secrets_flag = True
        
        if not hardcoded_secrets_flag:    
            impact = metadata.get('impact')
            if impact == 'HIGH':
                high_vulns += 1
            elif impact == 'MEDIUM':
                medium_vulns += 1

with open('gitleaks-report.json', 'r') as f:
    data = json.load(f)
    high_vulns += len(data)

with open('gosec-report.json', 'r') as f:
    data = json.load(f)
    for issue in data["Issues"]:
        if issue["details"] != "Potential hardcoded credentials":
            if issue["severity"] == "HIGH":
                high_vulns += 1
            elif issue["severity"] == "MEDIUM":
                medium_vulns += 1
    try:
        for error in data["Golang errors"]["rules"]:
            severity_levels = {imp['severity'] for imp in error['impacts']}
            if "HIGH" in severity_levels:
                high_vulns += 1
            elif "MEDIUM" in severity_levels and "HIGH" not in severity_levels:
                medium_vulns += 1
    except:
        print("Gosec Golang errors not found")

with open('govulncheck-report.json', 'r') as f:
    file_content = f.read()
    
    json_objects = file_content.split('\n}\n{\n')
    json_objects[0] = json_objects[0] + '\n}'
    json_objects[-1] = '{\n' + json_objects[-1]
    for i in range(1, len(json_objects)-1):
        json_objects[i] = '{\n' + json_objects[i] + '\n}'

    for json_str in json_objects:
        try:
            data = json.loads(json_str)
            if 'osv' in data:
                go_vulns += 1
        except json.JSONDecodeError:
            print("govulncheck not osv subJSON skipped")
            continue

print(f"The total number of high severity vulnerabilities in project: {high_vulns}")
print(f"The total number of medium severity vulnerabilities in project: {medium_vulns}")
print(f"The total number of vulnerabilities in current go libraries: {go_vulns}")
exit_code = 0
if high_vulns > 5:
    print("\033[91mThe number of high severity vulnerabilities is more than the acceptable threshold in: 5\033[0m")
    exit_code = 1

if medium_vulns > 10:
    print("\033[91mThe number of medium severity vulnerabilities is more than the acceptable threshold in: 10\033[0m")
    exit_code = 1
    
if go_vulns > 5:
     print("\033[91mThe number of vulnerabilities in the current version of go is too high, update the go version of your application\033[0m")
     exit_code = 1

if exit_code == 0:
    print("The number of vulnerabilities does not exceed the specified thresholds")
sys.exit(exit_code)