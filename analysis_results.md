# Результаты анализа исходного кода  

## GitLeaks  
Найдено 14 захардкоженых секретов в проекте:  

Identified a HashiCorp Terraform password field, risking unauthorized infrastructure configuration and security breaches.  
| Файл                     | Тип уязвимости      |
|--------------------------|---------------------|
|app/controllers/app.go:176|захардкоженный пароль|
|app/controllers/app.go:213|захардкоженный пароль|

Uncovered a JSON Web Token, which may lead to unauthorized access to web applications and sensitive user data.  
| Файл                      | Тип уязвимости          |
|---------------------------|-------------------------|
|app/controllers/app.go:152 | захардкоженный веб токен|
|app/controllers/app.go:199 | захардкоженный веб токен|
|app/controllers/app.go:236 | захардкоженный веб токен|
|TODO.md:32                 | захардкоженный веб токен|
|TODO.md:38                 | захардкоженный веб токен|
|TODO.md:44                 | захардкоженный веб токен|
|public/js/jwtnone.js:11    | захардкоженный веб токен|
|public/js/jwtnone.js:12    | захардкоженный веб токен|

Detected a Generic API Key, potentially exposing access to various services and sensitive operations. 
| Файл                      | Тип уязвимости           |
|---------------------------|--------------------------|
|app/controllers/app.go:144 | захардкоженный хеш пароля|
|app/controllers/app.go:191 | захардкоженный хеш пароля|
|app/controllers/app.go:228 | захардкоженный хеш пароля|
|conf/app.conf:31           | захардкоженный хеш пароля|

## GoSec  
GoSec - Это популярный и самый старый из актуальных анализаторов безопасности для Go. В его базе 35 уязвимостей, которые встречаются чаще всего. Все опасности, которые они несут, описаны в стандарте CVE. GoSec CI-интегрирован с платформами Github, Gitlab, Jenkins, Travis CI и другими. Проект поддерживает сообщество разработчиков Go.  

Проблем в некорретном использовании возможностей языка Go не выявлено, однако были обнаружены захардкоженные веб токены:  
details: "Potential hardcoded credentials"  
| Файл                      | Тип уязвимости           |
|---------------------------|--------------------------|
|app/controllers/app.go:152 | захардкоженный веб токен |
|app/controllers/app.go:199 | захардкоженный веб токен |
|app/controllers/app.go:236 | захардкоженный веб токен |

При подсчете quality, найденные через gosec уязвимости вида "details": "Potential hardcoded credentials" опускаются, так как дублируются с результатом работы GitLeaks  

## GoVulnCheck  
GoVulnCheck - разработка Go Security Team. Она развивает язык Go и лучше всех знает о его уязвимостях. В базе анализатора больше 340 уязвимостей, каждая описана в стандартах CVE и CWE. Go Vulnerability Manager умеет сканировать исходный код и уже собранные бинарные файлы. Он поможет узнать, насколько безопасны чужие программы.  

Найдено 28 потенциальных уязвимостей в используемых в проекте Go библиотеках:  
| Недостаток                                                                                            | Пакет                                 |
|-------------------------------------------------------------------------------------------------------|---------------------------------------|
|"Unsafe behavior in setuid/setgid binaries in runtime"                                                 | package: "stdlib"                     |
|"Path traversal on Windows in path/filepath"                                                           | package: "stdlib"                     |
|"Insecure parsing of Windows paths with a \\??\\ prefix in path/filepath"                              | package: "stdlib"                     |
|"Excessive memory allocation in net/http and net/textproto"                                            | package: "stdlib"                     |
|"Excessive resource consumption in net/http, net/textproto and mime/multipart"                         | package: "stdlib"                     |
|"Memory exhaustion in multipart form parsing in net/textproto and net/http"                            | package: "stdlib"                     |
|"Verify panics on certificates with an unknown public key algorithm in crypto/x509"                    | package: "stdlib"                     |
|"Panic on large handshake records in crypto/tls"                                                       | package: "stdlib"                     |
|"Large RSA keys can cause high CPU usage in crypto/tls"                                                | package: "stdlib"                     |
|"Before Go 1.20, the RSA based key exchange methods in crypto/tls may exhibit a timing side channel"   | package: "stdlib"                     |
|"Excessive resource consumption in mime/multipart"                                                     | package: "stdlib"                     |
|"Denial of service via chunk extensions in net/http"                                                   | package: "stdlib"                     |
|"Denial of service via crafted HTTP/2 stream in net/http and golang.org/x/net"                         | package: "stdlib"                     |
|"Insufficient sanitization of Host header in net/http"                                                 | package: "stdlib"                     |
|"Incorrect forwarding of sensitive headers and cookies on HTTP redirect in net/http"                   | package: "stdlib"                     |
|"HTTP/2 CONTINUATION flood in net/http"                                                                | package: "stdlib"                     |
|"Incorrect detection of reserved device names on Windows in path/filepath"                             | package: "stdlib"                     |
|"HTTP/2 rapid reset can cause excessive work in net/http"                                              | package: "stdlib"                     |
|"Backticks not treated as string delimiters in html/template"                                          | package: "stdlib"                     |
|"Improper sanitization of CSS values in html/template"                                                 | package: "stdlib"                     |
|"Improper handling of JavaScript whitespace in html/template"                                          | package: "stdlib"                     |
|"Improper handling of empty HTML attributes in html/template"                                          | package: "stdlib"                     |
|"Improper handling of HTML-like comments in script contexts in html/template"                          | package: "stdlib"                     |
|"Improper handling of special tags within script contexts in html/template"                            | package: "stdlib"                     |
|"Errors returned from JSON marshaling may break template escaping in html/template"                    | package: "stdlib"                     |
|"Incorrect privilege reporting in syscall and golang.org/x/sys/unix"                                   | package: "golang.org/x/sys"           |
|"Infinite loop in parsing in go/scanner"                                                               | package: "stdlib"                     |
|"Authorization bypass in github.com/dgrijalva/jwt-go"                                                  | package: "github.com/dgrijalva/jwt-go"|

Данные виды уязвимостей были вынесены в отдельную группу при подсчете quality. Обычно большое количество уязвимостей данного вида не воспроизводятся в конкретном проекте(особенно, если он небольшой). Однако в процессе роста проекта риски становятся более ощутимыми. Для избежания возникновения проблем рекомендуется своевременно обновление версии Go и прочих библиотек. Для примера данного проекта(версия Go=1.18) найдено 28 библиотечных недостатков, после тестового обновления проекта на версию Go=1.21 в результате работы GoVulnCheck не смог обнаружить ни 1 известной проблемы.  

## SemGrep  
High severity уязвимости не найдено, найдено 6 medium severity уязвимостей:  
| Файл                             | Тип уязвимости                                                                              |
|----------------------------------|---------------------------------------------------------------------------------------------|
|app/views/App/Auth1.html:41       | CWE-352: Cross-Site Request Forgery (CSRF)                                                  |
|app/views/App/Expired_JWT.html:33 | CWE-352: Cross-Site Request Forgery (CSRF)                                                  |
|app/views/App/Leaky_JWT.html:33   | CWE-352: Cross-Site Request Forgery (CSRF)                                                  |
|app/views/App/Timing.html:28      | CWE-352: Cross-Site Request Forgery (CSRF)                                                  |
|app/views/App/UserAgent.html:28   | CWE-352: Cross-Site Request Forgery (CSRF)                                                  |
|app/views/header.html:14          | CWE-79: Improper Neutralization of Input During Web Page Generation ('Cross-site Scripting')|

Также была найдена уязвимость conf/app.conf:31 - CWE-798: Use of Hard-coded Credentials, но при подсчете quality она была опущена, так как дублируется с результатом работы GitLeaks.  


## DAST  
Обнаружено 10 алертов:
| Тип уязвимости                                                                  | Критичность           |
|---------------------------------------------------------------------------------|-----------------------|
|[Absence of Anti-CSRF Tokens](https://www.zaproxy.org/docs/alerts/10202/)        |      - Medium (Low)   |
|[Anti-CSRF Tokens Check](https://www.zaproxy.org/docs/alerts/20012/)             |      - Medium (Medium)|
|[Bypassing 403](https://www.zaproxy.org/docs/alerts/40038/)                      |      - Medium (Medium)|
|[CSP: Wildcard Directive](https://www.zaproxy.org/docs/alerts/10055-4/)          |      - Medium (High)  |
|[Proxy Disclosure](https://www.zaproxy.org/docs/alerts/40025/)                   |      - Medium (Medium)|
|[Cookie Without Secure Flag](https://www.zaproxy.org/docs/alerts/10011/)         |      - Low (Medium)   |
|[Cookie without SameSite Attribute](https://www.zaproxy.org/docs/alerts/10054/)  |      - Low (Medium)   |
|[Permissions Policy Header Not Set](https://www.zaproxy.org/docs/alerts/10063-1/)|      - Low (Medium)   |
|[Private IP Disclosure](https://www.zaproxy.org/docs/alerts/2/)                  |      - Low (Medium)   |
|[Timestamp Disclosure - Unix](https://www.zaproxy.org/docs/alerts/10096/)        |      - Low (Low)      |

## DependencyCheck  
Уязвимых зависимостей не обнаружено  